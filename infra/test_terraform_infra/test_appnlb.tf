resource "yandex_alb_backend_group" "backend-testgroup" {
  name                     = "backendtestgroup"
  depends_on = [
    yandex_compute_instance_group.ig-test
  ]
  session_affinity {
    connection {
      source_ip = true
    }
  }

  http_backend {
    name                   = "backendtest"
    weight                 = 1
    port                   = 8080
    target_group_ids       = [yandex_compute_instance_group.ig-test.application_load_balancer.0.target_group_id]
    load_balancing_config {
      panic_threshold      = 90
    }    
    healthcheck {
      timeout              = "10s"
      interval             = "2s"
      healthy_threshold    = 10
      unhealthy_threshold  = 15
      http_healthcheck {
        path               = "/"
      }
    }
  }
}

resource "yandex_alb_http_router" "tf-testrouter" {
  name   = "httptestrouter"
  labels = {
    tf-label    = "tf-label-value"
    empty-label = ""
  }
}

resource "yandex_alb_virtual_host" "test-virtual-host" {
  name           = "testvirtualhost"
  http_router_id = yandex_alb_http_router.tf-testrouter.id
  route {
    name = yandex_alb_http_router.tf-testrouter.name
    http_route {
      http_route_action {
        backend_group_id = yandex_alb_backend_group.backend-testgroup.id
        timeout          = "30s"
      }
    }
  }
}    

resource "yandex_alb_load_balancer" "test-testbalancer" {
  name        = "applbtest"
  network_id  = "${data.yandex_vpc_network.finalvpc.id}"
  depends_on = [
    yandex_compute_instance_group.ig-test
  ]
  allocation_policy {
    location {
      zone_id   = var.zone1a
      subnet_id = yandex_vpc_subnet.finaltestnet.id
    }
  }

  listener {
    name = "listener01"
    endpoint {
      address {
        external_ipv4_address {
        }
      }
      ports = [ 80, 8080 ]
    }
    http {
      handler {
        http_router_id = yandex_alb_http_router.tf-testrouter.id
      }
    }
  }

}