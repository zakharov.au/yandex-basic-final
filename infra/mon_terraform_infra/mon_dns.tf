data "yandex_dns_zone" "primary" {
    name = "devride-ru"
}

resource "yandex_dns_recordset" "mon1" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "mon.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "grafana" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "grafana.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance.grafana.network_interface.0.nat_ip_address]
}