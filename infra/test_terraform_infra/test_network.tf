# resource "yandex_vpc_network" "finalvpc" {
#   name = var.vpc_name
# }
data "yandex_vpc_network" "finalvpc" {
    name = var.vpc_name
}
resource "yandex_vpc_subnet" "finaltestnet" {
  name           = var.finaltest_net
  zone           = var.zone1a
  network_id     = data.yandex_vpc_network.finalvpc.id
  v4_cidr_blocks = var.test_cidr
}



