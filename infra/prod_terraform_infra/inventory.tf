
# resource "null_resource" "add_hosts_inventory_for_ansible_infra" {
#   # triggers = {
#   #   always_run = "${timestamp()}"
#   # }
#   provisioner "local-exec" {
#     command = "echo \"[docker_servers] \ndocker_1 ansible_host=${yandex_compute_instance_group.ig-1.instances.0.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519 \ndocker_2 ansible_host=${yandex_compute_instance_group.ig-1.instances.1.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519 \n[mon_servers] \nmonitor ansible_host=${yandex_compute_instance.monitoring.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519\n[grafana] \ngrafana ansible_host=${yandex_compute_instance.grafana.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519 \" > ../ansible_infra/hosts"
#     environment = {
#       SRV1 = yandex_compute_instance_group.ig-1.instances.0.network_interface.0.nat_ip_address
#       SRV2 = yandex_compute_instance_group.ig-1.instances.1.network_interface.0.nat_ip_address
#      }
#   }
#   depends_on = [yandex_alb_load_balancer.test-balancer, yandex_compute_instance_group.ig-1, yandex_compute_instance.monitoring]
# }

# resource "null_resource" "copy_inventory_file_for_ansible_service" {
#   # triggers = {
#   #   always_run = "${timestamp()}"
#   # }
#   provisioner "local-exec" {
#     command = "cp ../ansible_infra/hosts ../../service/"
#   }
#   depends_on = [null_resource.add_hosts_inventory_for_ansible_infra]
# }
