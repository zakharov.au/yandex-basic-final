

resource "null_resource" "run_ansible_playbook" {
  provisioner "local-exec" {
    command = "cd ../ansible_gitagent; sleep 60; ansible-playbook all.yaml"
  }
  depends_on = [null_resource.add_hosts_inventory_for_ansible_gitagent]
}
