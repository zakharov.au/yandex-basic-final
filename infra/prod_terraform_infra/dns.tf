data "yandex_dns_zone" "primary" {
    name = "devride-ru"
}

resource "yandex_dns_recordset" "rs1" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "app.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_alb_load_balancer.test-balancer.listener.0.endpoint.0.address.0.external_ipv4_address.0.address]
}

# resource "yandex_dns_recordset" "mon1" {
#   zone_id = data.yandex_dns_zone.primary.id
#   name    = "mon.devride.ru."
#   type    = "A"
#   ttl     = 200
#   data    = [yandex_compute_instance.monitoring.network_interface.0.nat_ip_address]
# }

resource "yandex_dns_recordset" "host01" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "host01.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance_group.ig-1.instances.0.network_interface.0.nat_ip_address]
}

resource "yandex_dns_recordset" "host02" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "host02.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance_group.ig-1.instances.1.network_interface.0.nat_ip_address]
}

# resource "yandex_dns_recordset" "grafana" {
#   zone_id = data.yandex_dns_zone.primary.id
#   name    = "grafana.devride.ru."
#   type    = "A"
#   ttl     = 200
#   data    = [yandex_compute_instance.grafana.network_interface.0.nat_ip_address]
# }