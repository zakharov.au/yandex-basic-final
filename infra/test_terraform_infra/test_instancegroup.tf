resource "yandex_compute_instance_group" "ig-test" {
  name                = "igtest"
  folder_id           = var.folder_id
  service_account_id  = "${yandex_iam_service_account.testsa.id}"
  deletion_protection = false
  depends_on          = [yandex_resourcemanager_folder_iam_member.sa-testeditor]
  instance_template {
    #platform_id = "standard-v1"
    resources {
      cores         = var.vm_resources.core
      memory        = var.vm_resources.memory
      core_fraction = var.vm_resources.core_fraction
    }

    boot_disk {
      mode = "READ_WRITE"
      initialize_params {
        image_id = var.image_id
      }
    }
    scheduling_policy {
      preemptible = var.vm_premtible
    }
    network_interface {
      network_id = "${data.yandex_vpc_network.finalvpc.id}"
      subnet_ids = ["${yandex_vpc_subnet.finaltestnet.id}"]
      nat        = var.vm_nat
    }

    metadata = {
      serial-port-enable = var.metadata.serial-port-enable
      ssh-keys           = "ubuntu:${var.metadata.ssh-keys}"
   }
  }

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  allocation_policy {
    zones = ["ru-central1-a"]
  }

  deploy_policy {
    max_unavailable = 1
    max_expansion   = 0
  }
  application_load_balancer {
    target_group_name        = "target-testgroup"
    target_group_description = "load balancer target testgroup"
  }
  # load_balancer {
  #   target_group_name        = "target-group"
  #   target_group_description = "load balancer target group"
  # }
}

