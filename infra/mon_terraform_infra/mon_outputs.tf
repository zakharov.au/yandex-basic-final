
output "external_ip_monitoring" {
  value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
}

output "external_ip_grafana" {
  value = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
}