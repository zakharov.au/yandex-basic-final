resource "yandex_iam_service_account" "testsa" {
  name = "srcactest"
}

// Assigning roles to the service account
resource "yandex_resourcemanager_folder_iam_member" "sa-testeditor" {
  folder_id = var.folder_id
  role      = "editor"
  member    = "serviceAccount:${yandex_iam_service_account.testsa.id}"
}

resource "yandex_resourcemanager_folder_iam_member" "vpc-test-admin" {
  # Сервисному аккаунту назначается роль "vpc.publicAdmin".
  folder_id = var.folder_id
  role      = "vpc.publicAdmin"
  member    = "serviceAccount:${yandex_iam_service_account.testsa.id}"
}
