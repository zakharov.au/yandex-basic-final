data "yandex_dns_zone" "primary" {
    name = "devride-ru"
}

resource "yandex_dns_recordset" "testapp" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "testapp.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_alb_load_balancer.test-testbalancer.listener.0.endpoint.0.address.0.external_ipv4_address.0.address]
}


resource "yandex_dns_recordset" "host01" {
  zone_id = data.yandex_dns_zone.primary.id
  name    = "testhost01.devride.ru."
  type    = "A"
  ttl     = 200
  data    = [yandex_compute_instance_group.ig-test.instances.0.network_interface.0.nat_ip_address]
}

# resource "yandex_dns_recordset" "host02" {
#   zone_id = data.yandex_dns_zone.primary.id
#   name    = "testhost02.devride.ru."
#   type    = "A"
#   ttl     = 200
#   data    = [yandex_compute_instance_group.ig-test.instances.1.network_interface.0.nat_ip_address]
# }
