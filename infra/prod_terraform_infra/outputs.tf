output "external_ip_vm1" {
  value = yandex_compute_instance_group.ig-1.instances.0.network_interface.0.nat_ip_address
}

output "external_ip_vm2" {
  value = yandex_compute_instance_group.ig-1.instances.1.network_interface.0.nat_ip_address
}


output "external_nlb_id" {
  value = yandex_alb_load_balancer.test-balancer.id
}

output "external_nlb_ip" {
  value = yandex_alb_load_balancer.test-balancer.listener.0.endpoint.0.address.0.external_ipv4_address.0.address
}
# output "external_ip_monitoring" {
#   value = yandex_compute_instance.monitoring.network_interface.0.nat_ip_address
# }

# output "external_ip_grafana" {
#   value = yandex_compute_instance.grafana.network_interface.0.nat_ip_address
# }