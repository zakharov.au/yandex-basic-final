output "external_ip_testvm1" {
  value = yandex_compute_instance_group.ig-test.instances.0.network_interface.0.nat_ip_address
}

# output "external_ip_testvm2" {
#   value = yandex_compute_instance_group.ig-test.instances.1.network_interface.0.nat_ip_address
# }


output "external_nlbtest_id" {
  value = yandex_alb_load_balancer.test-testbalancer.id
}

output "external_nlbtest_ip" {
  value = yandex_alb_load_balancer.test-testbalancer.listener.0.endpoint.0.address.0.external_ipv4_address.0.address
}
