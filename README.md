# Сервис развертывания инфраструктуры в продуктивном и тестовом окружении
---
## Описание сервиса
---
Сервис предназначен для автоматизированного развертывания инфраструктуры для тестирования и состоит из следующих компонентов:
- план Terraform для развертывания продуктивной инфраструктуры в Yandex Cloud ./infra/prod_terraform_infra
- план Terraform для развертывания тестовой инфраструктуры в Yandex Cloud ./infra/test_terraform_infra
- план Terraform для развертывания инфраструктуры системы мониторинга в Yandex Cloud ./infra/mon_terraform_infra
- плейбук Ansible для подготовки развернутой инфраструктуры к запуску сервиса ./infra/ansible_infra/all.yml
- плейбук Ansible для запуска сервиса ./service/service.yml
- плейбук Ansible для запуска системы мониторинга и логирования ./infra/ansible_infra/monitorservers.yml
- плейбук Ansible для запуска агентов системы мониторинга и логирования ./infra/ansible_infra/monitor_node.yml
- план Terraform и плейбук Ansible для развертывания и регистрации Gitlab runner ./gitagent
---
## Требования к рабочей станции для развертывания
---
На рабочей станции для развертывания должны присутствовать следующие инструменты:
- Git https://git-scm.com/
- Terraform ( https://developer.hashicorp.com/terraform/downloads )
- Ansible ( https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html )
- провайдер Terraform для Yandex Cloud. Инструкция по установке https://cloud.yandex.ru/ru/docs/tutorials/infrastructure-management/terraform-quickstart
--
## Инструкции по установке
---
Для установки сервиса необходимо:
1. Cклонировать на рабочую станцию репозиторий
>git clone https://gitlab.com/zakharov.au/yandex-basic-final.git
2. Развернуть и зарегистрировать GitLab runner
3. Запустить план Terraform для развертывания продуктивной инфраструктуры
4. Запустить план Terraform для развертывания тестовой инфраструктуры
5. Запустить план Terraform для развертывания инфраструктуры системы мониторинга
6. Запустить плейбук CI pipeline для запуска сервиса
7. Настроить мониторинг

### 1. Запуск и регистрация GitLab runner
---
Для запуска и регистрации GitLab runner необходимо:
1. Указать значение переменной GITTOCKEN в роли для развертывания и регистрации Gitlab runner.
2. Перейти в директорию gitagent/terraform_gitagent.
3. Выполнить команду
>terraform init
4. Выполнить команду 
>terraform apply
5. Плэйбук ansible для регистрации GitLab runner запустится автоматически.


### 2. Запуск плана Terraform для развертывания продуктивной инфраструктуры в Yandex Cloud
---
Для запуска плана Terraform по развертыванию инфраструктуры в Yandex Cloud необходимо:
1. Перейти в директорию ./infra/prod_terraform_infra склонированного репозитория.
2. Выполнить команду 
>terraform init
3. Выполнить команду 
>terraform plan
4. Выполнить команду 
>terraform apply

### 3. Запуск плана Terraform для развертывания тестовой инфраструктуры в Yandex Cloud
---
Для запуска плана Terraform по развертыванию инфраструктуры в Yandex Cloud необходимо:
1. Перейти в директорию ./infra/test_terraform_infra склонированного репозитория.
2. Выполнить команду 
>terraform init
3. Выполнить команду 
>terraform plan
4. Выполнить команду 
>terraform apply

### 4. Запуск плана Terraform для развертывания инфраструктуры системы мониторинга в Yandex Cloud
---
Для запуска плана Terraform по развертыванию инфраструктуры системы мониторинга в Yandex Cloud необходимо:
1. Перейти в директорию ./infra/mon_terraform_infra склонированного репозитория.
2. Выполнить команду 
>terraform init
3. Выполнить команду 
>terraform plan
4. Выполнить команду 
>terraform apply

### 5. Запуск CI pipeline для развертывания сервиса
---
Для запуска CI pipeline необходимо:
1. Добавить переменную SSH_KEY (приватный ключ) на сервере GitLab зашифрованную в base64.
2. Перейти в директорию склонированного репозитория.
3. Ввыполнить команду 
>git add .
>git commit -m "namecommit"
>git push
4. CI pipeline для развертывания сервиса запустится автоматически.
5. Задания рарвертывания агентов мониторинга, серверов мониторинга и сервиса в продуктивном окружении запускаются вручную.

### 4. Выключение сервиса
---
Для выключения сервиса необходимо:
1. Перейти в соответствующую директорию продуктивного окружения склонированного репозитория.
2. Выполнить команду
>terraform destroy
3. Перейти в соответствующую директорию тестового окружения склонированного репозитория.
4. Выполнить команду
>terraform destroy
5. Перейти в соответствующую директорию с планом terraform системы мониторинга склонированного репозитория.
6. Выполнить команду
>terraform destroy
7. Перейти в директорию gitagent/terraform_gitagent склонированного репозитория.
8. Выполнить команду
>terraform destroy

### 5. Настройка мониторинга
---
Для настройки мониторинга необходимо:
1. Дождаться завершения развертывания сервиса при помощи CI pipeline.
2. Подключиться к интерфейсу управления Grafana - grafana.devride.ru:3000
3. Добавить источники данных mon.devride.ru:9090 и grafana.devride.ru:3100
4. Экспортировать дашборд для продуктивного окружения из файла Prod_dashboard-1710502925190.json
5. Экспортировать дашборд для тестового окружения из файла TEST_dashboard-1710502925190.json


## Дополнительная информация

Для смены версии сервиса необходимо поменять значение переменной version_app в файле main.yml, который находится по пути /service/service/defaults
