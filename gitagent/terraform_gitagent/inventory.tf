

resource "null_resource" "add_hosts_inventory_for_ansible_gitagent" {
  provisioner "local-exec" {
    command = "echo \"[agents] \nagent01 ansible_host=${yandex_compute_instance.gitagent.network_interface.0.nat_ip_address} ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/id_ed25519\" > ../ansible_gitagent/hosts"
    environment = {
      SRV1 = yandex_compute_instance.gitagent.network_interface.0.nat_ip_address
     }
  }
  depends_on = [yandex_compute_instance.gitagent]
}
