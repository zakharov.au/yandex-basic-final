resource "yandex_vpc_network" "gitagent" {
  name = var.vpc_name
}
resource "yandex_vpc_subnet" "gitagent" {
  name           = var.vpc_name
  zone           = var.default_zone
  network_id     = yandex_vpc_network.gitagent.id
  v4_cidr_blocks = var.default_cidr
}


data "yandex_compute_image" "ubuntu" {
  family = var.image_name
}

resource "yandex_compute_instance" "gitagent" {
  name        = local.name_vm
  platform_id = "standard-v1"
  hostname = local.name_vm
  resources {
    cores         = var.vm_resources.core
    memory        = var.vm_resources.memory
    core_fraction = var.vm_resources.core_fraction
  }
  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.ubuntu.image_id
      size = 8
    }
  }
  scheduling_policy {
    preemptible = var.vm_premtible
  }
  network_interface {
    subnet_id = yandex_vpc_subnet.gitagent.id
    nat       = var.vm_nat
  }

  metadata = {
    serial-port-enable = var.metadata.serial-port-enable
    ssh-keys           = "ubuntu:${var.metadata.ssh-keys}"
  }

}
